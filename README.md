# Ubuntu webserver

- Project just to maintain tutorials  

## How to Secure Your Server


### Add a Limited User Account


Create the user, replacing example_user with your desired username. You’ll then be asked to assign the user a password:

```bash  

adduser example_user

```
Add the user to the sudo group so you’ll have administrative privileges:  

```bash  

adduser example_user sudo

```

### SSH Daemon OptionsPermalink


Disallow root logins over SSH. This requires all SSH connections be by non-root users. Once a limited user account is connected, administrative privileges are accessible either by using sudo or changing to a root shell using su -.

**/etc/ssh/sshd_config**
```bash 

...
PermitRootLogin no

```

Restart the SSH service to load the new configuration. 

```bash 

sudo systemctl restart sshd

```

## How to Install a LAMP Stack on Ubuntu 18.04  

### Before You Begin

Update your system:  

```bash  

sudo apt update && sudo apt upgrade  

```   

### Install Packages Separately  


```bash  

sudo apt-get -y update && sudo apt-get -y upgrade && \
    sudo apt-get -y install nano apache2 php git unzip \
    libapache2-mod-php php-mysql php-mbstring php-intl php-simplexml php-curl php-json && \
    sudo a2enmod rewrite && \
    sudo apt-get -y clean && sudo apt-get -y autoclean && \
    sudo rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/*  

```  

## Configuration  

### Apache  

Open the apache2.conf Apache config file and adjust the KeepAlive setting:  

**/etc/apache2/apache2.conf**
```nano 

KeepAlive On
MaxKeepAliveRequests 50
KeepAliveTimeout 5

```

Note
The MaxKeepAliveRequests setting controls the maximum number of requests during a persistent connection. 50 is a conservative amount; you may need to set this number higher depending on your use-case. The KeepAliveTimeout setting controls how long the server waits (measured in seconds) for new requests from already connected clients. Setting this to 5 will avoid wasting RAM.  

The default multi-processing module (MPM) is the prefork module. mpm_prefork is the module that is compatible with most systems. Open the mpm_prefork.conf file located in /etc/apache2/mods-available and edit the configuration. Below are the suggested values for a 2GB Linode:  

**/etc/apache2/mods-available/mpm_prefork.conf**
```nano   

<IfModule mpm_prefork_module>
        StartServers            4
        MinSpareServers         3
        MaxSpareServers         40
        MaxRequestWorkers       200
        MaxConnectionsPerChild  10000
</IfModule> 

```  

Disable the event module and enable prefork:  

```bash  

sudo a2dismod mpm_event
sudo a2enmod mpm_prefork  

``` 

Restart Apache:  

```bash  

sudo systemctl restart apache2

```
 
### MySQL   

Use the mysql_secure_installation tool to configure additional security options. This tool will ask if you want to set a new password for the MySQL root user, but you can skip that step:

```bash  

sudo mysql_secure_installation  

``` 

Answer Y at the following prompts:

Remove anonymous users?
Disallow root login remotely?
Remove test database and access to it?
Reload privilege tables now?  


### PHP  

Edit the configuration file located in /etc/php/7.2/apache2/php.ini to enable more descriptive errors, logging, and better performance. The following modifications provide a good starting point:  

```nano  

error_reporting = E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR
max_input_time = 30
error_log = /var/log/php/error.log  

``` 

Create the log directory for PHP and give ownership to the Apache system user:

```bash  

sudo mkdir /var/log/php
sudo chown www-data /var/log/php  

``` 

Restart Apache:  

```bash  

sudo systemctl restart apache2  

``` 

Note
If you plan on using your LAMP stack to host a WordPress server, install additional PHP modules: sudo apt install php-curl php-gd php-mbstring php-xml php-xmlrpc  


### Troubleshooting  

If the site does not load at all, check if Apache is running, and restart it if required:


```bash  

sudo systemctl status apache2
sudo systemctl restart apache2  

``` 


## Configure Users 

```bash  

sudo adduser --home /home/username username
sudo mkdir -p /home/username/{site.com.br,logs}

```

## Configure virtual hosts 

### Disable default website  

```bash 

sudo a2dissite *default  

```

### Enable your new virtual host  

```bash  

sudo a2ensite example.com.conf  

```

  



