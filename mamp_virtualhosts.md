## MAMP Configuration Virtual Hosts


### First add your name to hosts file  

```bash  
sudo nano /etc/hosts 
```

```
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting. Do not change this entry.
##
127.0.0.1 localhost
127.0.0.1 mydomainname.local
255.255.255.255 broadcasthost
::1 localhost 
```

### vhost file  

```bash
nano  /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf
```

Add your virtual host configuration  


```
<VirtualHost *:80>    
    DocumentRoot "/Library/WebServer/Documents"
    ServerName localhost
</VirtualHost>
```
```
<VirtualHost *:80>    
    DocumentRoot "/Library/WebServer/Documents/laravel-test/public"
    ServerName laravel-test.dev
</VirtualHost>
```
```
<VirtualHost *:80>
    DocumentRoot "/Library/WebServer/Documents/vhost-test"
    ServerName vhost-test.dev
</VirtualHost>
```

### Enable extras vhosts in Apache global conf

```bash
nano /Applications/MAMP/conf/apache/httpd.conf
```

Uncomment this line  

```
# Virtual hosts
Include /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf  
``` 

### Restart Mamp  

Restart Mamp. Be sure have a project or index.html file inside your project folder. By browsing to the domain that you configured your project will show. That’s it.  



